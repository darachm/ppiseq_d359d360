module load java/11.0.11 || bash "module not loaded... not on sher?"

./nextflow scripts/run_pipeline.nf -c scripts/config.nfconfig \
			-resume -ansi-log false -with-dag reports/dag.html \
            --input_fastqs '/oak/stanford/projects/sflevy/seq_data/ppiseq/{20220718_novogene_d359bc_CKDL220015963-1A_HHMCVCCX2,20210820_novogene_dme359dme360_CKDL210017942-1a_HGKNJCCX2,20220812_novogene_d359bc_CKDL220015963-1A_HHMFCCCX2}/*gz' \
            --horf_barcode_map 'data/dme368_horf_barcode_mapping_both_230926.csv' \
            --vorf_barcode_map 'data/dme368_vorf_barcode_mapping_both_230926.csv' \
			--sample_sheets 'data/*_sample_sheet.csv' \
			--control_barcodes 'data/*_control_barcodes.csv' \
            -profile common,sher


            #--input_fastqs '/oak/stanford/projects/sflevy/seq_data/ppiseq/20220718_novogene_d359bc_CKDL220015963-1A_HHMCVCCX2/d359bc_CKDL220015963-1A_HHMCVCCX2_L4_*gz' \
			#--input_fastqs '/oak/stanford/projects/sflevy/seq_data/ppiseq/20210820_novogene_dme359dme360_CKDL210017942-1a_HGKNJCCX2/Whole_Lane_CKDL210017942-1a_HGKNJCCX2_L4_*gz' \

			#-work-dir w/ork \
