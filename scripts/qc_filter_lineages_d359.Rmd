---
title: 'Calling PPIs, and doing some QC analysis of d359'
author: 'darachm'
date: '`Sys.Date()`'
---

```{r,libs,echo=F,warning=F}
library(tidyverse)
library(magrittr) # tee pipe!
#library(parallel) # faster!
#library(ggrepel) # plots!
#library(viridis) # colors!
#library(feather) # for caching, since markdown caching seems bonkers right now?
library(DBI) # for sqlite
#library(egg) # figures

knitr::opts_chunk$set(cache.lazy=FALSE,message=FALSE) 

#rmarkdown::render('qc_filter_lineages_d359.Rmd',output_dir=getwd(),knit_root_dir=getwd())
```

```{r,reading_in_arguments,echo=F,warning=F}
# This bit just just making an args vector so if the arguments are specified
# (say during development), they are then.
try(rm(args))
args <- commandArgs(trailingOnly = TRUE)
print(args)
if (!length(args)) args <- c(
    "fitseq_meanfit_A.csv,fitseq_meanfit_B.csv,fitseq_meanfit_C.csv",
    "d359_analysis.sqlite",
    "scratch.sqlite"
    )

fitseq_mean_files <- str_split(args[1],",")[[1]]
sqlite_analysis_file <- args[2]
sqlite_scratch_file  <- args[3]
```

First some useful functions

```{r,useful_funcs}
source("misc_funcs.R")
```

# Setting up databases

```{r,sqlitesetup}

db <- dbConnect(RSQLite::SQLite(),"filtered.sqlite")
dbExecute(db,paste0("ATTACH DATABASE 'file:",sqlite_analysis_file,"?mode=ro' AS fixed"))
dbExecute(db,paste0("ATTACH DATABASE 'file:",sqlite_scratch_file,"?mode=ro' AS fixed_scratch"))

dbGetQuery(db,"SELECT * FROM unfiltered_sumd LIMIT 5")
dbGetQuery(db,"SELECT * FROM rep_counts LIMIT 5")
dbGetQuery(db,"SELECT * FROM fitseq_out LIMIT 5")

dbGetQuery(db,"SELECT * FROM sample2timerep")
dbGetQuery(db,"SELECT * FROM horfs LIMIT 5")
dbGetQuery(db,"SELECT * FROM vorfs LIMIT 5")
dbGetQuery(db,"SELECT * FROM controls LIMIT 5")

dbGetQuery(db,"SELECT * FROM annotated_barcodes LIMIT 5")
dbGetQuery(db,"SELECT count(g) FROM annotated_barcodes")

dbGetQuery(db,"SELECT * FROM annotated_barcodes_typed LIMIT 5")
dbGetQuery(db,"SELECT count(g) FROM annotated_barcodes_typed LIMIT 5")

dbGetQuery(db," SELECT * FROM total_counts ")
dbGetQuery(db," SELECT * FROM rep_prop limit 5")
dbGetQuery(db," SELECT * FROM modeled_counts_props limit 5")
dbGetQuery(db,"select * from summarizing limit 10") %>% head(30)

```

# QC

## Total counts per experiment, sample

```{r,total coverage counts per,cache=T}

totals <- dbGetQuery(db,"
        SELECT Rep as rep,Timepoint,sum(counts) AS total_counts from rep_prop
        GROUP BY Rep,Timepoint
        ") %>% as_tibble()
totals

g <- totals %>%
    group_by(rep) %>% summarize(total_counts=sum(total_counts)) %>% 
    ggplot()+theme_bw()+
    aes(x=rep,y=total_counts,fill=rep)+
    geom_bar(stat='identity')+guides(col='none')+
    NULL
g

fig_saver(g,"total_counts_per_rep",width=6,height=6)

g <- totals %>%
    ggplot()+theme_bw()+
    aes(x=Timepoint,y=total_counts,col=rep,group=rep)+
    geom_point()+geom_line()+
    scale_y_continuous(limits=c(0,NA))+
    NULL
g

fig_saver(g,"total_counts_per_rep_timepoint",width=8,height=6)

```

write this 

## Did FitSeq work?

### Line goes up

```{r,mean_fitness_fitseq,cache=T}

mean_fitnesses <- fitseq_mean_files %>% 
    map(read_csv) %>% bind_rows() 
mean_fitnesses

g <- mean_fitnesses %>%
    ggplot()+
    theme_classic()+
    aes(x=Samples,y=Estimate_Mean_Fitness,col=rep)+
    geom_line(linetype="dotted",position="identity")+
    geom_point()+
    ylab("Mean fitness, from fitseq")+
    xlab("Timepoint")+
    NULL
g

fig_saver(g,"qc_mean_fitness",width=5,height=5)
#ggsave(paste0(Sys.Date(),"_qc_mean_fitness.jpeg"),g,width=5,height=5)
```

Ah good, the mean fitness goes up. This is important!
If it goes down, that means you've used fitseq wrong and need to re-run with
more iterations, tighter standard for stopping.

### Fitness distribution looks sane

```{r,distribution_of_fitnesses,cache=T}

g <- dbGetQuery(db," SELECT rep,fit FROM fitseq_out") %>% 
    sample_n(1e6) %>%
    mutate(fit=as.numeric(fit)) %>%
    ggplot()+theme_classic()+
    aes(x=fit)+
    facet_wrap(~rep)+
    geom_histogram(bins=2e2)+
    xlab("Fitness of each lineage")+
    ylab("Count")+
    ggtitle("Fitness distribution, before filtering")+
    NULL
g

fig_saver(g,"qc_fitness_distribution",width=5,height=5)

```

It's not weirdly distributed or (too) spikey. This is important! If fitseq
has not run enough, it'll look very strange. The spikey stuff on either end
is indicative of fitseq delivering fitnesses for tiny counts data, 
like things dropping out. 
That's important to include for the simplex norm that fitseq does (frequency
of lineage at each timepoint), but we will filter it out right quick.

## Relationship of intial counts and fitness fit

```{r,counts vs fitness get datar,cache=T}
if ( ! 'filtering_dev' %in% dbListTables(db) ) {
#dbExecute(db,"DROP TABLE filtering_dev")
dbExecute(db,"
        CREATE TABLE filtering_dev AS
        SELECT * FROM 
            (
            SELECT * 
            FROM summarizing_by_ppi
            )
        LEFT JOIN
            (
            SELECT g,orfF,mutF,orfR,mutR,typeF,typeR,
                typeF||'_'||typeR AS type
            FROM annotated_barcodes_typed
            )
        USING (orfF,mutF,orfR,mutR,typeF,typeR)
        LEFT JOIN
            (
            SELECT rep,g,CAST(fit AS NUMERIC) AS fit 
            FROM fitseq_out
            )
        USING (g,rep)
        LEFT JOIN 
            ( 
            SELECT DISTINCT g,rep,counts AS counts_0 
            FROM rep_prop 
            WHERE Timepoint == 0 
            )
        USING (g,rep)
        LEFT JOIN
            (
            SELECT 
                Rep AS rep,g,r2_counts AS r2,
                sd_x_counts AS sd_observed,
                sd_y_counts AS sd_modeled
            FROM summarizing 
            )
        USING (g,rep)
    ") %>% head()
}
dbGetQuery(db,"PRAGMA table_info(filtering_dev)")
dbGetQuery(db," SELECT count(*) from filtering_dev LIMIT 10 ") 
dbGetQuery(db," SELECT * from filtering_dev LIMIT 10 ") %>% glimpse()
#dbGetQuery(db," SELECT * from filtering_dev WHERE typeF != 'vorf' LIMIT 10 ") 
dbGetQuery(db," SELECT * from fitseq_out LIMIT 10 ") 
dbGetQuery(db," SELECT * from summarizing_by_ppi LIMIT 10 ") 
dbGetQuery(db," SELECT * from filtering_dev ORDER BY rep,orfF,mutF,orfR,mutR LIMIT 10 ") 
dbGetQuery(db," SELECT * from filtering_dev WHERE counts_0 < min_counts_0 LIMIT 10 ") 
#dbGetQuery(db," SELECT * from summarizing_by_ppi WHERE typeF != 'vorf' LIMIT 10 ") 
#dbGetQuery(db," SELECT DISTINCT g,counts FROM rep_prop WHERE Timepoint == 0 limit 5")
dbGetQuery(db," SELECT count(g),* from filtering_dev GROUP BY orfF,mutF,orfR,mutR LIMIT 10 ")

```

```{r,get counts0 vs fitness table,cache=T}
filtering_dev <- dbReadTable(db,"filtering_dev") %>% 
    as_tibble() %>%
    unite(type,typeF,typeR,remove=F) %>%
    mutate(across(c(ends_with('counts_0'),'fit'),as.numeric))
filtering_dev

filtering_dev %>% filter(typeF!='vorf')
```

Describing datar

Taking subset of up to 1e5 of each type

```{r,counts vs fitness plot 1,cache=T}

label_one_line <- function(x){label_value(x,multi_line=F)}

pdatar <- filtering_dev  %>% 
    filter(!grepl("unann",type),!type%in%c('NA_NA','f123_dhfr')) %>%
    group_by(type) %>%
    sample_n(min(1e5,nrow(cur_data()))) %>%
    group_by(rep,orfF,mutF,orfR,mutR,typeF,typeR,type) %>%
    mutate(across(c('fit','r2'),c(mean=mean,median=median)))
pdatar

#pdatar_by_ppi <- pdatar %>% 
#    filter(type%in%c('null_horf','vorf_horf')) #%>% select(-g,-fit,-counts_0,-r2,-starts_with('sd_')) %>% distinct 
#pdatar_by_ppi

pg <- function(x){
    ggplot(x)+theme_bw()+
    geom_point(size=0.1,alpha=0.5)+
    geom_density2d(aes(group=F),col='red',size=0.2)+
    stat_smooth()+
    guides(col=guide_legend(override.aes=list(alpha=1,size=1)))+
    facet_wrap(~type,labeller=label_one_line)+
    NULL
    }
#g

# singles

g <- pdatar  %>% pg + aes(x=counts_0,y=fit)+ 
    scale_x_log10(limits=c(0.8,NA),oob=scales::squish)
g

g <- pdatar  %>% pg + aes(x=r2,y=fit)+ 
    scale_x_log10(limits=c(0.1,1),oob=scales::squish)
g

g <- pdatar %>% pg + aes(x=counts_0,y=r2)+ 
    scale_x_log10(limits=c(0.8,NA),oob=scales::squish)+
    scale_y_log10(limits=c(0.1,1),oob=scales::squish)
g

# aggregates before filtering

g <- pdatar %>% 
    filter(type%in%c('null_horf','vorf_horf')) %>%
    select(min_counts_0,fit_mean,r2_mean) %>% distinct() %>%
    pg + aes(x=min_counts_0,y=fit_mean)+ 
    scale_x_log10(limits=c(0.8,NA),oob=scales::squish)
g

g <- pdatar %>% 
    filter(type%in%c('null_horf','vorf_horf')) %>%
    select(min_counts_0,fit_mean,r2_mean) %>% distinct() %>%
    pg + aes(x=r2_mean,y=fit_mean)+ 
    scale_x_log10(limits=c(0.1,1),oob=scales::squish)
g

g <- pdatar %>% 
    filter(type%in%c('null_horf','vorf_horf')) %>%
    select(min_counts_0,fit_mean,r2_mean) %>% distinct() %>%
    pg + aes(x=min_counts_0,y=r2_mean)+ 
    scale_x_log10(limits=c(0.8,NA),oob=scales::squish)+
    scale_y_log10(limits=c(0.1,1),oob=scales::squish)
g



#fig_saver(
#    g + facet_wrap(~type,labeller=label_one_line) 
#    ,"initial_counts_vs_fitness",width=16,height=12)




```


# Filtering

Gonna develop a filter based on minimum counts and R^2 with the fitseq model

```{r,testing filters,cache=T}

counts0thresholds <- c(0,5,10,15,20)

g <- pdatar %>% 
    filter(type=='vorf_horf') %>%
    mutate(counts0thresholds_passed=unlist(map(min_counts_0,
                                function(x){sum(x>=counts0thresholds)}))) %>%
    mutate(group=map(counts0thresholds_passed,
                    function(x){ 
                        counts0thresholds[1:x]
                    })) %>%
    unnest(group) %>%
    filter(!is.na(group)) %>%
    ggplot()+theme_bw()+
    geom_point(size=0.1,alpha=0.5)+
    #geom_density2d(aes(group=F),col='blue',size=0.1)+
    stat_smooth()+
    scale_x_log10(limits=c(0.8,NA),oob=scales::squish)+
    guides(col=guide_legend(override.aes=list(alpha=1,size=1)))+
    NULL
#g

g + aes(x=counts_0,y=fit_mean)+ facet_grid(group~rep,labeller=label_one_line)+
    geom_vline(xintercept=counts0thresholds,linetype='dashed',col='red')

g + aes(x=r2,y=fit_mean)+ facet_grid(group~rep,labeller=label_one_line)+
    scale_y_log10(limits=c(0.8,NA),oob=scales::squish)+
    scale_x_log10(limits=c(0.1,NA))

g + aes(x=min_counts_0,y=fit_mean)+ facet_grid(group~rep,labeller=label_one_line)+
    geom_vline(xintercept=counts0thresholds,linetype='dashed',col='red')

g + aes(x=r2_mean,y=fit_mean)+ facet_grid(group~rep,labeller=label_one_line)+
    scale_y_log10(limits=c(0.8,NA),oob=scales::squish)+
    scale_x_log10(limits=c(0.1,NA))


fig_saver(
    g + aes(x=counts_0,y=fit)+ facet_grid(group~rep,labeller=label_one_line)
    ,"initial_counts_vs_fitness_thresholding",width=12,height=16)

fig_saver(
    g + aes(x=min_counts_0,y=fit)+ facet_grid(group~rep,labeller=label_one_line)
    ,"initial_min_counts_vs_fitness_thresholding",width=12,height=16)







```

```{r,r2 vs fitness,cache=T}

r2thresholds <- c(0,0.1,0.3,0.5,0.7)

g <- pdatar %>% 
    filter(type=='vorf_horf') %>%
    mutate(r2thresholds_passed=unlist(map(r2,
                                        function(x){sum(x>=r2thresholds,na.rm=T)}))) %>%
    mutate(group=map(r2thresholds_passed,
                    function(x){ 
                        r2thresholds[1:x]
                    })) %>%
    unnest(group) %>%
    filter(!is.na(group)) %>%
    ggplot()+theme_bw()+
    geom_point(size=0.1,alpha=0.5)+
    #geom_density2d(aes(group=F),col='blue',size=0.1)+
    stat_smooth()+
    scale_x_log10(limits=c(0.8,NA),oob=scales::squish)+
    guides(col=guide_legend(override.aes=list(alpha=1,size=1)))+
    NULL
#g

g + aes(x=counts_0,y=fit_mean)+ facet_grid(group~rep,labeller=label_one_line)

g + aes(x=r2,y=fit_mean)+ facet_grid(group~rep,labeller=label_one_line)+
    scale_y_log10(limits=c(0.8,NA),oob=scales::squish)+
    scale_x_log10(limits=c(0.1,NA))

g + aes(x=min_counts_0,y=fit_mean)+ facet_grid(group~rep,labeller=label_one_line)

g + aes(x=r2_mean,y=fit_mean)+ facet_grid(group~rep,labeller=label_one_line)+
    scale_y_log10(limits=c(0.8,NA),oob=scales::squish)+
    scale_x_log10(limits=c(0.1,NA))


fig_saver(
    g + aes(x=counts_0,y=fit)+ facet_grid(group~rep,labeller=label_one_line)
    ,"initial_counts_vs_fitness_thresholding_r2",width=12,height=16)

fig_saver(
    g + aes(x=min_counts_0,y=fit)+ facet_grid(group~rep,labeller=label_one_line)
    ,"initial_min_counts_vs_fitness_thresholding_r2",width=12,height=16)



```


```{r,counts vs fitness plot 2,cache=T}

pdatar_vorf <-filtering_dev  %>% 
    filter(grepl("vorf_horf",type)) %>%
    group_by(orfF) %>%
    sample_n(min(c(1e4,length(cur_group_rows())))) 
pdatar_vorf

g <- pdatar_vorf  %>% 
    ggplot()+theme_bw()+
    aes(x=counts_0,y=fit,col=interaction((min_counts_0>=05),(r2>=0.5)) )+
    geom_point(size=0.1,alpha=1.0)+
    geom_density2d(aes(group=F),col='black',size=0.1)+
    stat_smooth()+
    facet_wrap(~orfF)+
    scale_x_log10()+
    guides(col=guide_legend(override.aes=list(alpha=1,size=1)))+
    #geom_vline(xintercept=thresholds,linetype='dashed',col='red')+
    NULL
#g

fig_saver(g,"initial_counts_vs_fitness_per_vorf",width=16,height=16)

```

```{r,counts vs fitness plot 3,cache=T}

g <- pdatar %>%
    filter(typeF=='vorf',typeR %in% c('horf','dhfr','null')) %>%
    group_by(rep,orfF,mutF,orfR,mutR,typeF,typeR,type) %>%
    summarize(mean_fit=mean(fit),
        across(c('min_counts_0','mean_counts_0'),unique)) %>%
    ggplot()+theme_bw()+
    geom_point(size=0.1,alpha=1.0)+
    geom_density2d(aes(group=F),col='red',size=0.1)+
    stat_smooth()+
    #scale_x_log10(limit=c(0.8,NA),oob=scales::squish_infinite)+
    scale_x_log10(oob=scales::squish_infinite)+
    guides(col=guide_legend(override.aes=list(alpha=1,size=1)))+
    #geom_vline(xintercept=c(20,30),linetype='dashed',col='red')+
    theme(panel.grid.minor=element_blank())+
    NULL
#    g+facet_wrap(~type+filter_keep,labeller=function(x){label_value(x,multi_line=F)})+
#        aes(x=min_counts_0,y=mean,col=rep)+
#        xlab("min counts 0")+ylab("mean fitness of group")


#g+aes(x=mean_counts_0,y=mean,col=rep)

fig_saver( 
    g+facet_wrap(~type)+aes(x=mean_counts_0,y=mean_fit)+
        xlab("mean counts 0")+ylab("mean fitness of group")
    ,"mean_initial_counts_vs_mean_fitness_per_vorf",width=12,height=12)

fig_saver( 
    g+facet_wrap(~type)+aes(x=min_counts_0,y=mean_fit)+
        xlab("min counts 0")+ylab("mean fitness of group")
    ,"min_initial_counts_vs_mean_fitness_per_type_filter",width=12,height=12)

fig_saver( 
    g+facet_wrap(~orfF)+aes(x=mean_counts_0,y=mean_fit)+
        xlab("mean counts 0")+ylab("mean fitness of group")
    ,"mean_initial_counts_vs_mean_fitness_per_vorf",width=12,height=12)

fig_saver( 
    g+facet_wrap(~orfF)+aes(x=min_counts_0,y=mean_fit)+
        xlab("min counts 0")+ylab("mean fitness of group")
    ,"min_initial_counts_vs_mean_fitness_per_type_filter",width=12,height=12)


g <- pdatar %>%
    filter(type %in% c('horf_vorf','vorf_dhfr','vorf_null','null_horf')) %>%
    mutate(filter=counts_0>=05 & r2>=0.5) %>%
    group_by(rep,orfF,mutF,orfR,mutR,typeF,typeR,type,filter) %>%
    summarize(mean_fit=mean(fit),
        across(c('min_counts_0','mean_counts_0'),unique)) %>%
    ggplot()+theme_bw()+
    geom_point(size=0.1,alpha=1.0)+
    geom_density2d(aes(group=F),col='red',size=0.1)+
    stat_smooth()+
    scale_x_log10(limit=c(0.8,NA),oob=scales::squish_infinite)+
    guides(col=guide_legend(override.aes=list(alpha=1,size=1)))+
    #geom_vline(xintercept=c(20,30),linetype='dashed',col='red')+
    theme(panel.grid.minor=element_blank())+
    NULL
#    g+facet_wrap(~type+filter_keep,labeller=function(x){label_value(x,multi_line=F)})+
#        aes(x=min_counts_0,y=mean,col=rep)+
#        xlab("min counts 0")+ylab("mean fitness of group")
    g+facet_wrap(~type)+aes(x=min_counts_0,y=mean_fit,col=filter)+
        xlab("min counts 0")+ylab("mean fitness of group")



```

```{r,looking for bias in the filtering criteria,cache=T}

g <- pdatar %>% 
    filter(counts_0 >= 05, r2 > 0.5) %>% 
    filter(type%in%c('null_horf','vorf_horf')) %>%
    select(counts_0,r2,min_counts_0,fit_mean,r2_mean) %>% distinct() %>%
    pg + aes(x=counts_0,y=fit_mean)+ 
    scale_x_log10(limits=c(NA,NA),oob=scales::squish)
g

g <- pdatar %>% 
    filter(counts_0 >= 05, r2 > 0.5) %>% 
    filter(type%in%c('null_horf','vorf_horf')) %>%
    select(counts_0,r2,min_counts_0,fit_mean,r2_mean) %>% distinct() %>%
    pg + aes(x=r2,y=fit_mean)+ 
    scale_x_log10(limits=c(NA,NA),oob=scales::squish)
g

pdatar %>% 
    group_by(counts_0 >= 05, r2 > 0.5) %>% 
    tally()

```




# Applying filter to generate analysis data subset

Now we apply this across the dataset.
Note that this is just setting a `filter_keep` variable, 
for if we keep it or not.


```{r,filter_apply_criteria,cache=T}

#dbExecute(db," DROP TABLE filtered ") 
if ( ! 'filtered' %in% dbListTables(db) ) {
    dbExecute(db,"
        CREATE TABLE filtered AS
        SELECT * FROM
            (
            SELECT * FROM
                ( SELECT rep,g,1 as filter_pass FROM 
                    ( select * from filtering_dev 
                        WHERE ( r2 >= 0.5 AND counts_0 > 5 ) OR 
                            (typeF == 'f123' AND typeR == 'null') ) 
                UNION
                SELECT rep,g,0 as filter_pass FROM 
                    ( select * from filtering_dev 
                        WHERE ( r2 < 0.5 OR counts_0 <= 5 ) AND 
                            NOT (typeF == 'f123' AND typeR == 'null') ) 
                )
            LEFT JOIN
                ( SELECT rep,g,CAST(fit AS NUMERIC) AS fit FROM fitseq_out )
            USING (g,rep)
            LEFT JOIN
                (
                SELECT g,
                    orfF,orfR,
                    mutF,mutR,
                    typeF,typeR,
                    typeF||'_'||typeR AS type
                FROM annotated_barcodes_typed
                )
            USING (g)
            )
        ") %>% head()
}
dbGetQuery(db,"SELECT * FROM filtered limit 10") %>% glimpse() 
dbGetQuery(db,"SELECT count(*),filter_pass FROM filtered where typeF == 'f123' AND typeR == 'null' group by filter_pass ")



# checking that each barcoded control is only represented once, not in both
# the filter in and filter out 
#dbGetQuery(db,"SELECT count(*),rep,filter_pass,g FROM filtered where typeF == 'f123' AND typeR == 'null' group by rep,filter_pass,g ")

```

And then produce the `analysis_datar` object that I like to use in the
calling script:

```{r,make analysis datar object,cache=T}

if ( !dbExistsTable(db,"analysis_datar") ) {
#dbExecute(db," DROP TABLE analysis_datar ") 
dbExecute(db,"
        CREATE TABLE analysis_datar AS
        SELECT DISTINCT 
            g,rep,
            CAST(fit AS NUMERIC) AS fit,
            CAST(filter_pass AS INT) AS filter_pass,
            orfF,orfR,
            genotypeF,genotypeR,
            mutF,mutR,
            flagF,flagR,
            CAST(flagF_use AS INT) AS flagF_use,
            CAST(flagR_use AS INT) AS flagR_use,
            typeF,typeR 
        FROM 
            ( SELECT g,rep,filter_pass,fit FROM filtered )
        LEFT JOIN
            ( SELECT 
                    g,barcode_reverse,barcode_forward,typeF,typeR,
                    typeF||'_'||typeR AS type
                FROM annotated_barcodes_typed )
        USING (g)
        LEFT JOIN
            (
            SELECT 
                barcode_reverse,orfR,genotypeR,flagR,new_mutations AS mutR,
                flagR NOT IN ('nonsense','missense','low_coverage_mm') AS flagR_use
            FROM horfs 
            UNION
            SELECT 
                barcode AS barcode_reverse,
                type AS orfR, type AS genotypeR, 
                NULL AS mutR,
                'matches' AS flagR, 1 AS flagR_use
            FROM controls 
            )
        USING (barcode_reverse)
        LEFT JOIN
            (
            SELECT 
                barcode_forward,orfF,genotypeF,flagF,new_mutations AS mutF,
                flagF NOT IN ('nonsense','missense','low_coverage_mm') AS flagF_use
            FROM vorfs 
            UNION
            SELECT 
                barcode AS barcode_forward,
                type AS orfF, type AS genotypeF, 
                NULL AS mutF,
                'matches' AS flagF, 1 AS flagF_use
            FROM controls 
            )
        USING (barcode_forward)
    ") %>% head()
}
dbGetQuery(db," SELECT * from analysis_datar LIMIT 10 ") %>% glimpse()
dbGetQuery(db," SELECT count(*) from analysis_datar ") 
dbGetQuery(db," SELECT distinct(typeF) from analysis_datar LIMIT 10 ") %>% glimpse()
dbGetQuery(db," SELECT distinct(typeR) from analysis_datar LIMIT 10 ") %>% glimpse()

dbGetQuery(db," SELECT count(g) from analysis_datar group by g LIMIT 10 ") %>% glimpse()

```

I need to write a little section to export this for supplements, tidy new sqlite

How many were filtered out? What types?

```{r,summarize filtering,cache=T}

sum_datar <- dbGetQuery(db,"
        SELECT 
            count(*) as n,
            rep,filter_pass,typeF,typeR,flagF,flagR,flagF_use,flagR_use
        FROM analysis_datar
        GROUP BY rep,filter_pass,typeF,typeR,flagF,flagR,flagF_use,flagR_use
    ") %>%
    unite('type',c(typeF,typeR),sep='_') %>%
    mutate(across(matches('flag[FR]_use'),function(x){ifelse(is.na(x),0,x)}))

sum_datar %>% 
    group_by(
        rep,filter_pass,type,flagF_use,flagR_use
        ) %>%
    summarize(n=sum(n)) %>% 
    select(type,flagF_use,flagR_use,filter_pass,rep,n) %>%
    arrange(type,flagF_use,flagR_use,filter_pass,rep) %>%
    knitr::kable()

g <- sum_datar %>% 
    mutate(
        filter_pass=ifelse(filter_pass,"passed filter","failed filter"),
        usable=ifelse(flagF_use&flagR_use,"wildtype protein","mutated")
        ) %>%
    group_by(
        rep,filter_pass,usable,type
        ) %>%
    summarize(n=sum(n)) %>%
    ggplot()+theme_bw()+
    aes(x=type,y=n,shape=rep,col=filter_pass)+
    facet_grid(~usable,
        scales="fixed",labeller=function(x){label_value(x,multi_line=F)})+
    geom_point(position=position_dodge())+
    theme(axis.text.x=element_text(angle=90))+
    scale_y_log10()+
    NULL
g

fig_saver(g,"lineages_passing_filters_by_many",width=12,height=8)

g <- sum_datar %>% 
    mutate(
        filter_pass=ifelse(filter_pass,"passed filter","failed filter"),
        usable=ifelse(flagF_use&flagR_use,"wildtype protein","mutated")
        ) %>%
    group_by(
        rep,filter_pass,usable,type
        ) %>%
    filter(usable=='wildtype protein') %>%
    summarize(n=sum(n)) %>%
    ggplot()+theme_bw()+
    aes(x=type,y=n,shape=rep,col=filter_pass)+
    geom_point(position=position_dodge())+
    theme(axis.text.x=element_text(angle=90))+
    scale_y_log10()+
    NULL
g

fig_saver(g,"lineages_passing_filters_wildtype",width=8,height=5)

```

So... I lose some lineages this way. Which makes sense, this was setup
as a partial screen (scant depth, select and quant winners).
There may also be a lot of crap in there, unknown chimeras and other small
counts noise that I want to filter out.

<!--
maybe put in this analysis, had done for previous rewrite, but upon refactor...

one day!

## Unknown barcode lineages look similar but a bit less abundant
-->

## Additional handy tables for analysis

Making those here so don't have to for the calling etc script

```{r,wide counts,cache=T}
if ( !dbExistsTable(db,"wide_counts") ) {
#dbExecute(db,"DROP TABLE wide_counts")
dbExecute(db,"
    CREATE TABLE wide_counts AS
    SELECT
        g,rep,
            sum(case when timepoint = '0' then counts end) as 'counts_0',
            sum(case when timepoint = '1' then counts end) as 'counts_1',
            sum(case when timepoint = '2' then counts end) as 'counts_2',
            sum(case when timepoint = '3' then counts end) as 'counts_3',
            sum(case when timepoint = '4' then counts end) as 'counts_4',
            sum(case when timepoint = '0' then counts end)/total_counts as 'counts_0_prop',
            sum(case when timepoint = '1' then counts end)/total_counts as 'counts_1_prop',
            sum(case when timepoint = '2' then counts end)/total_counts as 'counts_2_prop',
            sum(case when timepoint = '3' then counts end)/total_counts as 'counts_3_prop',
            sum(case when timepoint = '4' then counts end)/total_counts as 'counts_4_prop'
    FROM
        (
        SELECT * FROM
            (
            SELECT 
                g,replicate AS rep,counts,
                    substr(time_date,0,instr(time_date,'_')) AS timepoint
            FROM
                (
                SELECT 
                    *,substr(sample,instr(sample,'_')+1,1000) AS time_date
                FROM 
                    (select * from rep_counts )
                )
            )
        LEFT JOIN
            ( SELECT Rep AS rep,Timepoint as timepoint,total_counts FROM total_counts )
        USING (rep,timepoint)
        )
    GROUP BY
        g,rep
    ORDER BY 
        g,rep
    ") 
}
dbGetQuery(db," SELECT * from wide_counts LIMIT 10 ") 
dbGetQuery(db," SELECT count(*) from wide_counts LIMIT 10 ") 
#dbExecute(db," DROP TABLE wide_counts ") 
```


```{r,lineages,cache=T,eval=F,echo=F}
# I don't know if this is actually of any use, so commenting out
#if ( !dbExistsTable(db,"lineages") ) {
#
#            case when  = '0' then counts end) as 'counts_0',
#
#dbGetQuery(db,"
#    SELECT 
#        *,typeF||'_'||typeR AS type,
#            CASE WHEN flagF IN ('matches','synonymous') then 1 else 0 end AS flagF_use,
#            CASE WHEN flagR IN ('matches','synonymous') then 1 else 0 end AS flagR_use
#    FROM 
#        (
#        SELECT * FROM 
#            ( select * from filtered limit 10000 )
#        LEFT JOIN
#            (SELECT g,genotypeF,genotypeR,flagF,flagR FROM annotated_barcodes_typed limit 10000 )
#        USING (g)
#        )
#    ") %>% glimpse()
#
#dbGetQuery(db,"
#        SELECT * FROM 
#            ( SELECT g,Rep AS rep,filter_keep FROM 
#                (   SELECT * FROM filter_keep 
#                    UNION SELECT * FROM filter_out )
#                )
#        LEFT JOIN
#            (SELECT * FROM annotated_barcodes_typed )
#        USING (g)
#        LEFT JOIN
#            (SELECT * FROM fitseq_out )
#        USING (g,rep)
#        ") %>%
#    as_tibble() %>%
#    mutate(across(c(
#                'fit','fit_se','ll',
#                starts_with('Modeled')
#                ),as.numeric)) %>%
#    unite(type,typeF,typeR,remove=F) %>%
#    mutate_at( 
#        .vars=vars(starts_with('flag')) , 
#        .funs=list(use=function(x) {x %in% c('matches','synonymous') } )  ) %>%
#    mutate(fit=fit+1) %>%
#    {dbWriteTable(db, 'lineages',.)}
#}
#dbGetQuery(db," SELECT * from lineages LIMIT 10 ") 
##dbExecute(db," DROP TABLE lineages ") 
```

