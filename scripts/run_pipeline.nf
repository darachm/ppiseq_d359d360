#!/usr/bin/env nextflow
nextflow.enable.dsl=2

//
// Introduction and preliminaries 
//

// dirs for output, reports, inspection links 
[ "./tmp", "./output", "./reports" ].each{ file(it).mkdirs() }

//
// workflow execution 
//

// This is a helper to define a variable you can access in the shell scripts
// This one just defines the string to put as argument
// Note it splits the memory evenly, but subtracts 1 to prevent overruns
def buildSortArguments(cpus,memory,n_sorts) {
    cpus_per_sort = (cpus / n_sorts).toInteger()
    memory_per_sort = ((memory.toString() - / GB/).toFloat() / n_sorts).toInteger() - 1 
    if (memory_per_sort == 0) return "ERROR exit ERROR its asking for zero mem"
    return '--parallel='+cpus_per_sort+' -T ./ -S '+memory_per_sort+'G'
}

workflow {

    // this is reading in the filenames, splitting them to IDs / read
    input_fastqs = Channel.fromPath(params.input_fastqs)
        | map{ 
            (file, sample, lane, type, read) = (it =~ /([^\/]+)_([^_]+)_([RrIi]?)(\d+).*\.f.*z/)[0] ;
                // Type is to handle read and index fastqs from custom 
                // demuxing, not needed for d359360 run
            [ read, sample, it] ; 
            } 

    // reading in sample sheets, of course
    sample_sheets = Channel.fromPath(params.sample_sheets)
        | map{ 
            (match, experiment) = (it =~ /([^\/]+?)_[^\/]+$/)[0] ;
            [ experiment, it] ; 
            } 

    // reading in control barcodes, of course
    control_barcodes = Channel.fromPath(params.control_barcodes)
        | map{ 
            (match, experiment) = (it =~ /([^\/]+?)_[^\/]+$/)[0] ;
            [ experiment, it] ; 
            } 

    // Cutting up the fastqs with itermae, d359 and d360
    chopped_reads_d359 = input_fastqs
        | combine(
            Channel.from(
                [ "1", file('scripts/mjtF.yaml'), ["d359","d360"] ] ,
                    // Outputs with both exp tags, so can use the
                    // read 1 barcodes parsed using d359 format
                    // in d360 as well, in next section
                [ "2", file('scripts/evoR2.yaml'), ["d359"] ] ,
                )
            ,by: [0]) // adding yaml config files
        | itermae // running, makes SAM file with multiple codes per read

    chopped_reads_d359
        | itermae_tabulate_result

    // From this, we split these into different files and compress them
    // to make it less disk-impactful
    base_tsv_gz = chopped_reads_d359 
        | split_compress

    // Then feed this into each clustering run, with different parameters for
    // different barcode sizes
    clust_inner = base_tsv_gz
        | combine( Channel.from( ['-r 5 -d 1'] ) ) 
        | starcode_inner 
    clust_i5 = base_tsv_gz
        | combine( Channel.from( ['-r 5 -d 2'] ) ) 
        | starcode_i5 
    clust_i7 = base_tsv_gz
        | combine( Channel.from( ['-r 5 -d 2'] ) ) 
        | starcode_i7
    clust_geno = base_tsv_gz
        | combine( Channel.from( ['-r 5 -d 3'] ) ) 
        | starcode_genotype 

    // These I additionally cluster to our reference annotations in order to
    // align little mismatches to our annotations.
clust_geno_with_barcode_map = clust_geno 
//    output: tuple val(experiment), val(id), val(read), 
//        path("${id}_${read}_genotype_clustered.tsv.gz")
        //| groupTuple( by: [0,2] )
        | map{ it[2,0,1,3] }
        | combine( 
            Channel.fromList(
                [
                    ['1', file(params.vorf_barcode_map)],
                    ['2', file(params.horf_barcode_map)],
                    ]
                ) 
            , by:[0])
        | map{ it[1,0,2..4] }
//    tuple val(experiment), val(read), val(id),
//        path("clustered_genotypes.tsv.gz"), path(prev_codes)

clustered_geno = clust_geno_with_barcode_map
        | groupTuple( by: [0,1] )
//    tuple val(experiment), val(read), [id],
//        [clustered_genotypes.tsv.gz], [codes]
        | map{ it[0..3]+[it[4].unique()] }
//    input: tuple val(experiment), val(read), val(id),
//        path(clustered_genotype_tsv_gz), path(prev_codes)
        | cluster_barcodes_found_and_annotated
//    output: tuple val(experiment), val(read), 
//        path("clustered_genotypes_map.tsv"),
//        path("barcode_map_${read}.tsv")

    corrected_geno = clust_geno_with_barcode_map
//    tuple val(experiment), val(read), val(id),
//        path("clustered_genotypes.tsv.gz"), path(prev_codes)
        | combine( clustered_geno , by:[0,1] )
//    tuple val(experiment), val(read), val(id),
//        path("clustered_genotypes.tsv.gz"), path(prev_codes), 
//        path(map), path(barcode_map_corrected)
        | correct_barcodes_found_and_annotated
//    output: tuple val(experiment), val(id), val(read), 
//        path("${read}_genotype_clustered.tsv.gz"),
//        path("barcode_map_${read}.csv")

    // Here we merge together all the clustered codes into one file,
    // and we need a separate variable because we'll use it later in d360
    clustered_code = corrected_geno 
        | map{ it[0..3]}
        | mix(clust_inner, clust_i5, clust_i7)
        | transpose( by:[0] )

    // Get to counts - duplicate on UMIs and count up, then chimera correct
    deduped_barcode_table = clustered_code
        | groupTuple( by: [0,1] )
        | combine( base_tsv_gz.transpose(by:0).map{[it[0],it[1],it[3]]}.groupTuple( by: [0,1] ) ,by:[0,1] )
        | filter{ it[0] == "d359" }
        | merge_codes_to_counts_dedup_or_not 

    // Demux these files into chimera-adjusted tables
    deduped_barcode_table
        //| map{ it[0..2] }
        | combine( 
            Channel.fromList(
                [
                    ["d359",
                        [ file('scripts/tabulate_d359.ipynb'),
                            file('data/d359_sample_sheet.csv'),
                            file('data/d359_control_barcodes.csv') ]
                        ]
                    ]
                )
            , by:[0])
        | map{ it.flatten() }
        | tabulate_lineage_counts 
        | map{ it[0..2] }
//        | transpose(by: [1])
//        | map{ 
//            (file, rep) = (it[1] =~ /([^\/]+)_tabulated.csv/)[0] ;
//            [ it[0], rep, it[1] ] ;
//            } 
        | set{ tabulated_counts }

    each_rep_counts = tabulated_counts
        | groupTuple(by: [0])
        | combine( 
            Channel.fromList(
                [ ["d359", 
                    file('scripts/assemble_reps.ipynb'),
                    file('data/d359_exp_design.csv') ] ]
                )
            ,by: [0] )
        | assemble_rep_counts

    // And from there, fitseq each counts table
    fitseqd_table = each_rep_counts
        | transpose(by: [1])
        | map{ 
            (file, rep) = (it[1] =~ /([^\/]+)_counts.csv/)[0] ;
            [ it[0], rep, it[1] ] ;
            } 
        //input: tuple val(experiment), val(rep), path(csv)
        | fitseq_counts_table
        //output: tuple val(experiment), val(id), path("fitseq_out.csv"), path("fitseq_meanfit.csv")

    called_d359 = fitseqd_table
        | groupTuple(by: [0,1] )
        | combine( tabulated_counts | groupTuple(by: 0) | map{ it[0,2] } , by: [0] )
        | combine( each_rep_counts , by: [0] )
        // experiment, rep, fitseq_outcsv, fitseqmeanfit, [unfilt tabulations], 
        //     [per-rep counts]
        | combine( 
            clustered_geno 
                //    output: tuple val(experiment), val(read), 
                //        path("clustered_genotypes_map.tsv"),
                //        path("barcode_map_${read}.tsv")
                | map{ it[0,3]} | transpose(by:0) | groupTuple(by:[0]) 
                | map{ [it[0],it[1].sort({a,b -> a.name <=> b.name})] }
            , by: [0] )
        // experiment, rep, fitseq_outcsv, fitseqmeanfit, [unfilt tabulations], 
        //     [per-rep counts], [barcode_maps]
        | combine( 
            Channel.fromList(
                [
                    ["d359",
                        [   file('data/d359_sample_sheet.csv') ,
                            file('data/d359_control_barcodes.csv'),
                            file('data/human_ORFs.csv'),
                            file('data/addgene_sars2_gateway_nostop_plasmids.csv')
                            ]
                        ]
                    ]
                )
            , by:[0])
        // experiment, rep, fitseq_outcsv, fitseqmeanfit, [unfilt tabulations], 
        //     [per-rep counts], [barcode_maps], additional_files
        | groupTuple( by:[0] )
        | map{ it[0..1]+ it[2..7].collect{it.flatten().unique()} }
        // experiment, rep, fitseq_outcsv, fitseqmeanfit, [unfilt tabulations], 
        //     [per-rep counts], [barcode_maps], additional_files
        | assemble_sqlites
        | combine( Channel.fromList(
                [ ["d359", file('scripts/assemble_databases.Rmd'), 
                        file('scripts/misc_funcs.R') ] ]
                ) , by:[0])
        | build_scratch_sqlite
        | combine( Channel.fromList(
                [ ["d359", file('scripts/qc_filter_lineages_d359.Rmd'),
                        file('scripts/misc_funcs.R') ] ]
                ) , by:[0])
        | qc_filter_sqlite
        | combine( 
            clustered_geno 
                //    output: tuple val(experiment), val(read), 
                //        path("clustered_genotypes_map.tsv"),
                //        path("barcode_map_${read}.tsv")
                | map{ it[0,3]} | transpose(by:0) | groupTuple(by:[0]) 
            , by: [0] )
        | combine( 
            Channel.fromList(
                [
                    ["d359",
                        file('scripts/call_ppis_d359.Rmd'),
                        file('scripts/misc_funcs.R') ]
                    ]
                )
            , by:[0])
//    input: tuple val(experiment), val(reps), 
//        path(analysis_sqlite), path(fitseq_mean_csv), 
//        path(scratch_sqlite), path(filtered_sqlite),
//        path(barcode_maps), path(script), path(helper_script)
        | call_d359
//    output: tuple val(experiment), path("d359_ppis_called*.csv"), 
//        path(barcode_maps), path("*html"), path("*jpeg") //, path("*pdf")

    // Cutting up the fastqs with itermae, d360, but only on the ones that 
    // didn"t get parsed into d359 
    chopped_reads_d360 = chopped_reads_d359
        | filter{ it[1] =~ /HGKNJCCX2/ }
//    output: tuple val(experiment), val(id), val(read), 
//        path("${id}_${read}_pass.sam.gz"), path("failed.fastq")
        | map{ [it[2],it[1],it[4]] }
        | combine(
            Channel.from(
                [ "1", file('scripts/evoF1.yaml'), ["d360"] ] ,
                    // This one is to get the classic iSeq barcodes for the
                    // re-annotation data (barcoder on barcoder)
                [ "2", file('scripts/evoR1.yaml'), ["d360"] ] ,
                    // Classic iSeq2 barcodes, for parsing out the vORFs
                )
            ,by: [0]) // adding yaml config files
        | itermae2 // running, makes SAM file with multiple codes per read
            // need different name, because /nextflow/

    // This is for sharing with someone who gave us extra nextera libraries
    // to balance base diversity
    chopped_reads_d360 
        | publish_undetermined

    base_tsv_gz_d360 = chopped_reads_d360 
        | mix(chopped_reads_d359)
        | transpose(by: 0)
        | groupTuple( by: [0,1,2] )
        //    ,by: [0,1,2]) // mix in the mjtF parsed codes... 
        | filter{ it[1] =~ /HGKNJCCX2/ }
        | filter{ it[0] =~ /d360/ }
//    input: tuple val(experiment), val(id), val(read), 
//        path(chopped_sam), path(failed)
        | split_compress_d360 // if every have to re-run, rewrite with pigz

    base_tsv_gz_d360
        | combine( Channel.from( ['-r 5 -d 1'] ) ) 
        | starcode_inner_d360 
        | set{ clust_inner_d360 }
    base_tsv_gz_d360
        | combine( Channel.from( ['-r 5 -d 2'] ) ) 
        | starcode_i5_d360 
        | set{ clust_i5_d360 }
    base_tsv_gz_d360
        | combine( Channel.from( ['-r 5 -d 2'] ) ) 
        | starcode_i7_d360
        | set{ clust_i7_d360 }
    base_tsv_gz_d360
        | combine( Channel.from( ['-r 5 -d 3'] ) ) 
        | starcode_genotype_d360 
        | set{ clust_geno_d360 }

    clustered_code_d360 = 
        clust_geno_d360.mix(clust_inner_d360, clust_i5_d360, clust_i7_d360)
        //| transpose(by:0)

    merged_barcode_table_d360 = clustered_code_d360
        | groupTuple( by: [0,1] )
        | map{ [it[0],it[1],it[3]] }
        | merge_ends_d360

    merged_barcode_table_d360
        | combine( 
            Channel.fromList(
                [
                    ["d360",
                        file('scripts/analyze_table_d360.Rmd'),
                        [   file('data/d360_sample_sheet.csv'),
                            file('data/MATa_well_IDs.csv'),
                            file('data/MATalpha_well_IDs.csv'),
                            file(params.vorf_barcode_map),
                            file('data/addgene_sars2_gateway_nostop_plasmids.csv'),
                            file('data/d360_matings.csv'),
                            file('data/sars2_library_splits.csv')
                            ]
                        ]
                    ]
                )
            , by:[0])
        | analyze_d360 

}

// itermae! Requires arguments to be specified outside the fuction, so that's
// done in the object itermae_arguments
process itermae {
    time '16h'
    label 'half_core'
    label 'smol_mem'
    label 'itermae'
    input: tuple val(read), val(id), path(infq), path(itermae_conf), 
        val(experiment)
    output: tuple val(experiment), val(id), val(read), 
        path("${id}_${read}_pass.sam.gz"), path("failed.fastq")
    shell: '''
zcat -f !{infq} \
    | parallel --jobs !{task.cpus} --pipe -l 4 -N 10000 \
        'itermae --config !{itermae_conf} ' \
    | gzip > !{id}_!{read}_pass.sam.gz
'''
}

// itermae! Requires arguments to be specified outside the fuction, so that's
// done in the object itermae_arguments
process itermae2 {
    time '12h'
    label 'half_core'
    label 'smol_mem'
    label 'itermae'
    input: tuple val(read), val(id), path("in.fq"), path(itermae_conf), 
        val(experiment)
    output: tuple val(experiment), val(id), val(read), 
        path("${id}_${read}_pass.sam.gz"), path("failed.fastq")
    shell: '''
zcat -f in.fq \
    | parallel --jobs !{task.cpus} --pipe -l 4 -N 10000 \
        'itermae --config !{itermae_conf} ' \
    | gzip > !{id}_!{read}_pass.sam.gz
'''
}

process publish_undetermined { 
    publishDir 'output', mode:"rellink"
    memory '1G'
    cpus '4'
    time '2h'
    label 'bioinfmunger'
    input: tuple val(experiment), val(id), val(read), 
        path(samgz), path(failed_fastq)
    output: path("undetermined_${read}.fq.gz")
    shell: '''
cat !{failed_fastq} | pigz -cp !{task.cpus} > undetermined_!{read}.fq.gz
'''
}

process itermae_tabulate_result {
    publishDir 'output', mode:"rellink"
    time '10m'
    label 'one_core'
    memory '1GB'
    label 'bioinfmunger'
    input: tuple val(experiment), val(id), val(read), 
        path(pass_sam), path(fail_fq)
    output: path("${id}_${read}_itermaestats.tsv")
    shell: '''
echo "!{id}\t!{read}\t" \
        $(zcat -f !{pass_sam} | grep -v "^@" | wc -l) \
        "\t" \
        $(zcat -f !{fail_fq} | wc -l | xargs -I'{}' bash -c 'echo $(({}/4))') \
    > !{id}_!{read}_itermaestats.tsv
'''
}





process assemble_rep_counts { 
    publishDir 'output', mode: 'rellink'
    label 'jupyter'
    time '8h'
    label 'one_core'
    label 'half_mem'
    label 'bioinfmunger'
    input: tuple val(experiment), val(ids), path(tabulateds), 
        path(script), path(exp_design)
    output: tuple val(experiment), path("*_counts.csv")
    shell: '''
jupyter nbconvert --to=html --config="./dummy" --debug --ExecutePreprocessor.timeout=-1 --execute !{script}
'''
}
//mv tabulated_unfiltered.csv !{experiment}_!{id}_tabulated_unfiltered.csv


//


// split sam to tsv, compress
process split_compress { 
    publishDir 'tmp'
    time '2h'
    label 'half_core'
    label 'half_mem'
    label 'bioinfmunger'
    input: tuple val(experiment), val(id), val(read), 
        path(chopped_sam), path(failed)
    output: tuple val(experiment), val(id), val(read), 
        path("${id}_${read}.tsv.gz")
        //sort_parm_1 = buildSortParm(task.cpus,task.memory,1)
        //sort_parm_2 = buildSortParm(task.cpus,task.memory,2)
        //buildSortArguments
    shell: '''
export SORTPARM="--parallel=!{task.cpus} -T ./ -S !{((task.memory.toString() - / GB/).toInteger() - 1).toString()}G"
export PIGZCAT="pigz -cdfp !{task.cpus}"
${PIGZCAT} !{chopped_sam} \
    | mawk -F'[_\\t]' '{print $1"\\t"$2"\\t"$3"\\t"$4"\\tgenotype="$13}'  \
    | sed 's/description=.*:/i7=/' | sed 's/+/\\ti5=/' \
    | sort ${SORTPARM} -k1,1 \
    | pigz -cp !{task.cpus} \
    > !{id}_!{read}.tsv.gz
'''
}

// split sam to tsv, compress
process split_compress_d360 { 
    publishDir 'tmp'
    label 'all_core'
    label 'all_mem'
    time '4h'
    label 'bioinfmunger'
    input: tuple val(experiment), val(id), val(read), 
        path("chopped_sam_???"), path("failed_???")
    output: tuple val(experiment), val(id), val(read), 
        path("d360_${id}_${read}.tsv.gz")
        //sort_parm_1 = buildSortParm(task.cpus,task.memory,1)
        //sort_parm_2 = buildSortParm(task.cpus,task.memory,2)
    shell: '''
export SORTPARM="--parallel=!{task.cpus} -T ./ -S !{((task.memory.toString() - / GB/).toInteger() - 1).toString()}G"
export PIGZCAT="pigz -cdfp !{task.cpus}"
${PIGZCAT} chopped_sam_* \
    | mawk -F'[_\\t]' '{print $1"\\t"$2"\\t"$3"\\t"$4"\\tgenotype="$13}'  \
    | sed 's/description=.*:/i7=/' | sed 's/+/\\ti5=/' \
    | sort ${SORTPARM} -k1,1 \
    | pigz -cp !{task.cpus} \
    > d360_!{id}_!{read}.tsv.gz
'''
}

// starcode the genotype barcodes, correct back to the IDs
process starcode_genotype { 
    publishDir 'tmp'
    time '1h'
    label 'all_core'
    label 'all_mem'
    label 'starcode'
    input: tuple val(experiment), val(id), val(read), 
        path(tsv_gz), val(starcode_parms)
    output: tuple val(experiment), val(id), val(read), 
        path("${id}_${read}_genotype_clustered.tsv.gz")
    shell: '''
export SORTPARM="--parallel=!{task.cpus} -T ./ -S !{((task.memory.toString() - / GB/).toInteger() - 1).toString()}G"
export PIGZCAT="pigz -cdfp !{task.cpus}"
${PIGZCAT} !{tsv_gz} \
    | cut -f1,6 \
    | sed 's/\tgenotype=/\t/' \
    > input.tsv
cat input.tsv \
    | cut -f2 \
    | uniq -c \
    | awk '{print $2"\t"$1}' \
    | starcode --threads !{task.cpus} !{starcode_parms} --tidy  \
    | join -1 2 -2 1 input.tsv - \
    | cut -d' ' -f2,3 --output-delimiter="\t" \
    | pigz -cp !{task.cpus} \
    > !{id}_!{read}_genotype_clustered.tsv.gz
rm input.tsv
'''
}

// starcode the inner barcodes, correct back to the IDs
process starcode_inner { 
    publishDir 'tmp'
    time '1h'
    label 'all_core'
    label 'all_mem'
    label 'starcode'
    input: tuple val(experiment), val(id), val(read), 
        path(tsv_gz), val(starcode_parms)
    output: tuple val(experiment), val(id), val(read), 
        path("${id}_${read}_inner_clustered.tsv.gz")
    shell: '''
export PIGZCAT="pigz -cdfp !{task.cpus}"
${PIGZCAT} !{tsv_gz} \
    | cut -f1,3 \
    | sed 's/\tinterior=/\t/' \
    > input.tsv
cat input.tsv \
    | cut -f2 \
    | uniq -c \
    | awk '{print $2"\t"$1}' \
    | starcode --threads !{task.cpus} !{starcode_parms} --tidy  \
    | join -1 2 -2 1 input.tsv - \
    | cut -d' ' -f2,3 --output-delimiter="\t" \
    | pigz -cp !{task.cpus} \
    > !{id}_!{read}_inner_clustered.tsv.gz
rm input.tsv
'''
}

// starcode the i5 barcodes, correct back to the IDs
process starcode_i5 { 
    publishDir 'tmp'
    time '1h'
    label 'all_core'
    label 'all_mem'
    label 'starcode'
    input: tuple val(experiment), val(id), val(read), 
        path(tsv_gz), val(starcode_parms)
    output: tuple val(experiment), val(id), val(read), 
        path("${id}_${read}_i5_clustered.tsv.gz")
    shell: '''
export PIGZCAT="pigz -cdfp !{task.cpus}"
${PIGZCAT} !{tsv_gz} \
    | cut -f1,5 \
    | sed 's/\ti5=/\t/' \
    > input.tsv
cat input.tsv \
    | cut -f2 \
    | uniq -c \
    | awk '{print $2"\t"$1}' \
    | starcode --threads !{task.cpus} !{starcode_parms} --tidy  \
    | join -1 2 -2 1 input.tsv - \
    | cut -d' ' -f2,3 --output-delimiter="\t" \
    | pigz -cp !{task.cpus} \
    > !{id}_!{read}_i5_clustered.tsv.gz
rm input.tsv
'''
}

// starcode the i7 barcodes, correct back to the IDs
process starcode_i7 { 
    publishDir 'tmp'
    time '1h'
    label 'all_core'
    label 'all_mem'
    label 'starcode'
    input: tuple val(experiment), val(id), val(read), 
        path(tsv_gz), val(starcode_parms)
    output: tuple val(experiment), val(id), val(read), 
        path("${id}_${read}_i7_clustered.tsv.gz")
    shell: '''
export PIGZCAT="pigz -cdfp !{task.cpus}"
${PIGZCAT} !{tsv_gz} \
    | cut -f1,4 \
    | sed 's/\ti7=/\t/' \
    > input.tsv
cat input.tsv \
    | cut -f2 \
    | uniq -c \
    | awk '{print $2"\t"$1}' \
    | starcode --threads !{task.cpus} !{starcode_parms} --tidy  \
    | join -1 2 -2 1 input.tsv - \
    | cut -d' ' -f2,3 --output-delimiter="\t" \
    | pigz -cp !{task.cpus} \
    > !{id}_!{read}_i7_clustered.tsv.gz
rm input.tsv
'''
}

// starcode the genotype barcodes, correct back to the IDs
process starcode_genotype_d360 { 
    publishDir 'tmp'
    time '8h'
    label 'all_core'
    label 'all_mem'
    label 'starcode'
    input: tuple val(experiment), val(id), val(read), 
        path(tsv_gz), val(starcode_parms)
    output: tuple val(experiment), val(id), val(read), 
        path("d360_${id}_${read}_genotype_clustered.tsv.gz")
    shell: '''
export PIGZCAT="pigz -cdfp !{task.cpus}"
${PIGZCAT} !{tsv_gz} \
    | cut -f1,6 \
    | sed 's/\tgenotype=/\t/' \
    > input.tsv
cat input.tsv \
    | cut -f2 \
    | uniq -c \
    | awk '{print $2"\t"$1}' \
    | starcode --threads !{task.cpus} !{starcode_parms} --tidy  \
    | join -1 2 -2 1 input.tsv - \
    | cut -d' ' -f2,3 --output-delimiter="\t" \
    | pigz -cp !{task.cpus} \
    > d360_!{id}_!{read}_genotype_clustered.tsv.gz
rm input.tsv
'''
}

// starcode the inner barcodes, correct back to the IDs
process starcode_inner_d360 { 
    publishDir 'tmp'
    time '8h'
    label 'all_core'
    label 'all_mem'
    label 'starcode'
    input: tuple val(experiment), val(id), val(read), 
        path(tsv_gz), val(starcode_parms)
    output: tuple val(experiment), val(id), val(read), 
        path("d360_${id}_${read}_inner_clustered.tsv.gz")
    shell: '''
export PIGZCAT="pigz -cdfp !{task.cpus}"
${PIGZCAT} !{tsv_gz} \
    | cut -f1,3 \
    | sed 's/\tinterior=/\t/' \
    > input.tsv
cat input.tsv \
    | cut -f2 \
    | uniq -c \
    | awk '{print $2"\t"$1}' \
    | starcode --threads !{task.cpus} !{starcode_parms} --tidy  \
    | join -1 2 -2 1 input.tsv - \
    | cut -d' ' -f2,3 --output-delimiter="\t" \
    | pigz -cp !{task.cpus} \
    > d360_!{id}_!{read}_inner_clustered.tsv.gz
rm input.tsv
'''
}

// starcode the i5 barcodes, correct back to the IDs
process starcode_i5_d360 { 
    publishDir 'tmp'
    time '8h'
    label 'all_core'
    label 'all_mem'
    label 'starcode'
    input: tuple val(experiment), val(id), val(read), 
        path(tsv_gz), val(starcode_parms)
    output: tuple val(experiment), val(id), val(read), 
        path("d360_${id}_${read}_i5_clustered.tsv.gz")
    shell: '''
export PIGZCAT="pigz -cdfp !{task.cpus}"
${PIGZCAT} !{tsv_gz} \
    | cut -f1,5 \
    | sed 's/\ti5=/\t/' \
    > input.tsv
cat input.tsv \
    | cut -f2 \
    | uniq -c \
    | awk '{print $2"\t"$1}' \
    | starcode --threads !{task.cpus} !{starcode_parms} --tidy  \
    | join -1 2 -2 1 input.tsv - \
    | cut -d' ' -f2,3 --output-delimiter="\t" \
    | pigz -cp !{task.cpus} \
    > d360_!{id}_!{read}_i5_clustered.tsv.gz
rm input.tsv
'''
}

// starcode the i7 barcodes, correct back to the IDs
process starcode_i7_d360 { 
    publishDir 'tmp'
    time '8h'
    label 'all_core'
    label 'all_mem'
    label 'starcode'
    input: tuple val(experiment), val(id), val(read), 
        path(tsv_gz), val(starcode_parms)
    output: tuple val(experiment), val(id), val(read), 
        path("d360_${id}_${read}_i7_clustered.tsv.gz")
    shell: '''
export PIGZCAT="pigz -cdfp !{task.cpus}"
${PIGZCAT} !{tsv_gz} \
    | cut -f1,4 \
    | sed 's/\ti7=/\t/' \
    > input.tsv
cat input.tsv \
    | cut -f2 \
    | uniq -c \
    | awk '{print $2"\t"$1}' \
    | starcode --threads !{task.cpus} !{starcode_parms} --tidy  \
    | join -1 2 -2 1 input.tsv - \
    | cut -d' ' -f2,3 --output-delimiter="\t" \
    | pigz -cp !{task.cpus} \
    > d360_!{id}_!{read}_i7_clustered.tsv.gz
rm input.tsv
'''
}




process cluster_barcodes_found_and_annotated {
    publishDir 'tmp'
    time '4h'
    label 'half_core'
    label 'half_mem'
    label 'starcode'
    input: tuple val(experiment), val(read), val(ids),
        path(clustered_genotype_tsv_gz), path(prev_codes)
    output: tuple val(experiment), val(read), 
        path("clustered_genotypes_map.tsv"),
        path("barcode_map_${read}.csv")
    shell: 
        sort_args_1 = buildSortArguments(task.cpus,task.memory,1)
        sort_args_2 = buildSortArguments(task.cpus,task.memory,2)
        sort_args_3 = buildSortArguments(task.cpus,task.memory,3)
        '''#
tail -n+2 !{prev_codes} | cut -d, -f1 \
    | awk '{print $0;print $0;print $0;print $0;print $0}' \
    > clust_forward
echo $?
#
zcat -f !{clustered_genotype_tsv_gz} \
    | cut -f2 | sort !{sort_args_1} | uniq \
    >> clust_forward
echo $?
#
starcode --tidy -i clust_forward -s -d 3 -t 8 \
    | sort -k1,1 !{sort_args_1} | uniq \
    > clustered_genotypes_map.tsv
echo $?
#
head -n 1 !{prev_codes} > barcode_map_!{read}.csv
tail -n+2 !{prev_codes} \
    | sort !{sort_args_1} -k2,2 \
    | join -t, -1 1 -2 1 - <( cat clustered_genotypes_map.tsv | sed 's/\t/,/g' ) \
    | awk -F, '{print $7","$2","$3","$4","$5","$6}' \
    >> barcode_map_!{read}.csv
echo $?
'''
}

process correct_barcodes_found_and_annotated {
    publishDir 'tmp'
    time '2h'
    label 'half_core'
    label 'half_mem'
    label 'starcode'
    input: tuple val(experiment), val(read), val(id), 
        path(clustered_genotype_tsv_gz), path(prev_codes),
        path(map), path(barcode_map)
    output: tuple val(experiment), val(id), val(read), 
        path("${read}_genotype_clustered.tsv.gz"), path(barcode_map)
    shell: 
        sort_args_1 = buildSortArguments(task.cpus,task.memory,1)
        sort_args_2 = buildSortArguments(task.cpus,task.memory,2)
        sort_args_3 = buildSortArguments(task.cpus,task.memory,3)
        '''#
zcat -f !{clustered_genotype_tsv_gz} \
    | sort !{sort_args_3} -k2,2 \
    | join -t'	' -1 2 -2 1 - !{map} \
    | awk '{print $2"\t"$3}' \
    | sort !{sort_args_3} -k1,1 \
    | pigz -cp !{task.cpus} \
    > !{read}_genotype_clustered.tsv.gz
echo $?
'''
}


// Had to take out becasue it always got an error exit code from diff damn it
//diff \
//    <( cat barcode_map_!{read}.csv | sort -t, -k1,1 ) \
//    <( cat !{prev_codes} | sort -t, -k1,1 ) \
//    > diffz || echo "all good"



// merge double barcodes from each end
process merge_ends_d360 { 
    publishDir 'tmp'
    time '12h'
    label 'all_core'
    label 'all_mem'
    label 'bioinfmunger'
    input: tuple val(experiment), val(id), path("*")
    output: tuple val(experiment), val(id), 
        path("${id}_genotype_inner_i5_i7_clustered_merged.tsv.gz")
    shell: 
        sort_args_1 = buildSortArguments(task.cpus,task.memory,1)
        sort_args_2 = buildSortArguments(task.cpus,task.memory,2)
        '''
export LC_ALL=C
export PIGZCAT="pigz -cdfp !{task.cpus}"
join -t'	' -j 1 \
        <( ${PIGZCAT} *_1_*genotype*gz | sort !{sort_args_2} -k1b,1 ) \
        <( ${PIGZCAT} *_2_*genotype*gz  | sort !{sort_args_2} -k1b,1 ) \
    > genotypes.joined
join -t'	' -j 1 \
        <( ${PIGZCAT} *_1_*inner*gz | sort !{sort_args_2} -k1b,1  ) \
        <( ${PIGZCAT} *_2_*inner*gz  | sort !{sort_args_2} -k1b,1 ) \
    > inners.joined
join -t'	' -j 1 \
        <( ${PIGZCAT} *_1_*i5*gz | sort !{sort_args_2} -k1b,1 ) \
        <( ${PIGZCAT} *_2_*i5*gz | sort !{sort_args_2} -k1b,1 ) \
    > i5s.joined
join -t'	' -j 1 \
        <( ${PIGZCAT} *_1_*i7*gz | sort !{sort_args_2} -k1b,1 ) \
        <( ${PIGZCAT} *_2_*i7*gz | sort !{sort_args_2} -k1b,1 ) \
    > i7s.joined
join -t'	' -j 1 \
        genotypes.joined \
        <( join -t'	' -j 1 \
            inners.joined \
            <( join -t'	' -j 1 i5s.joined i7s.joined) \
            ) \
    | cut -f2- | sort !{sort_args_1} -k1b,1 | uniq -c \
    | sed 's/^\\s*\\([0-9]\\+\\)\\s\\+/\\1\\t/' \
    | pigz -cp !{task.cpus} \
    > !{id}_genotype_inner_i5_i7_clustered_merged.tsv.gz
    rm *joined
'''
}



// merge double barcodes from each end
process merge_codes_to_counts_dedup_or_not { 
    time '16h'
    label 'all_core'
    label 'all_mem'
    label 'bioinfmunger'
    input: tuple val(experiment), val(id), val(reads), path("*"), path("*")
    output: tuple val(experiment), val(id), 
        path("${id}_genotype_umi_inner_i5_i7_counts.tsv.gz"),
        path("${id}_genotype_umi_inner_i5_i7_counts_deduped.tsv.gz")
    shell: 
        sort_args_1 = buildSortArguments(task.cpus,task.memory,1)
        sort_args_2 = buildSortArguments(task.cpus,task.memory,2)
        '''
join -t'	' -j 1 \
    <( join -t'	' -j 1 \
        <( join -t'	' -j 1 <( zcat 1_genotype_clustered.tsv.gz ) <( zcat 2_genotype_clustered.tsv.gz ) ) \
        <( join -t'	' -j 1 \
            <( join -t'	' -j 1 \
                <( zcat !{id}_1.tsv.gz | cut -f1-2 | sed 's/umi=//g' ) \
                <( zcat !{id}_2.tsv.gz | cut -f1-2 | sed 's/umi=//g' ) \
                ) \
            <( join -t'	' -j 1 <( zcat *_1_*inner*gz ) <( zcat *_2_*inner*gz ) ) \
            )\
        )\
    <( join -t'	' -j 1 \
        <( join -t'	' -j 1 <( zcat *_1_*i5*gz ) <( zcat *_2_*i5*gz ) )\
        <( join -t'	' -j 1 <( zcat *_1_*i7*gz ) <( zcat *_2_*i7*gz ) )\
        ) \
    | cut -f2- | sort !{sort_args_2} | uniq -c \
    | sed 's/^\\s*\\([0-9]\\+\\)\\s\\+/\\1\\t/' \
    | tee >(pigz -cp !{task.cpus} \
                > !{id}_genotype_umi_inner_i5_i7_counts.tsv.gz ) \
    | cut -f2,3,6-11 \
    | sort !{sort_args_2} | uniq -c \
    | sed 's/^\\s*\\([0-9]\\+\\)\\s\\+/\\1\\t/' \
    | pigz -cp !{task.cpus} \
    > !{id}_genotype_umi_inner_i5_i7_counts_deduped.tsv.gz
'''
}

// merge double barcodes from each end
process take_uniq_umis_d360 { 
    publishDir 'tmp'
    time '4h'
    label 'all_core'
    label 'all_mem'
    label 'bioinfmunger'
    input: tuple val(experiment), val(id), path(umi_tsv_gz)
    output: tuple val(experiment), val(id), 
        path("${id}_genotype_inner_i5_i7_clustered_merged_deduped.tsv.gz"),
        path(umi_tsv_gz)
    shell: 
        sort_args_1 = buildSortArguments(task.cpus,task.memory,1)
        sort_args_2 = buildSortArguments(task.cpus,task.memory,2)
'''
export PIGZCAT="pigz -cdfp !{task.cpus}"
${PIGZCAT} !{umi_tsv_gz} | cut -f2,3,6-11 \
    | sort !{sort_args_1} | uniq -c \
    | sed 's/^\\s*\\([0-9]\\+\\)\\s\\+/\\1\\t/' \
    | pigz -cp !{task.cpus} \
    > !{id}_genotype_inner_i5_i7_clustered_merged_deduped.tsv.gz
'''
}


// tabulate and chimera correct
process tabulate_lineage_counts { 
    time '4h'
    label 'jupyter'
    label 'one_core'
    label 'all_mem'
    input: tuple val(experiment), val(id), 
        path(double_tsv_gz), path(umi_tsv_gz),
        path(script), path(sample_csv), path(controls_csv)
    output: tuple val(experiment), val(id), 
        path("${experiment}_${id}_tabulated_unfiltered.csv"),
        path("tabulate_d359.html"), path("*png")
    shell: '''
jupyter nbconvert --to=html --config="./dummy" --debug --ExecutePreprocessor.timeout=-1 --execute !{script}
mv tabulated_unfiltered.csv !{experiment}_!{id}_tabulated_unfiltered.csv
'''
}


// FitSeq
process fitseq_counts_table {
    publishDir 'output', mode: 'rellink'
    time '12h'
    label 'fitseq'
    label 'half_core'
    memory '16G'
    input: tuple val(experiment), val(rep), path(csv)
    output: tuple val(experiment), val(rep), 
        path("fitseq_out_${rep}.csv"), path("fitseq_meanfit_${rep}.csv")
    shell: '''
cat !{csv} | tail -n+2 | cut -d, -f3- > counts.csv
fitseq.py -i counts.csv -p !{task.cpus} \
        -t $(seq 0 $(head -n 1 !{csv} | tr ',' '\n' | tail -n+4 | wc -l) | xargs echo) \
        -m 100 --min-step 0.00001 \
        --min-iter 40 --max-chunk-size 10000 \
        --fitness-type m \
        --output-mean-fitness fitseq_meanfit.csv \
        2> fitseq_stdout.txt \
    | tail -n+2 \
    | paste -d',' <(cut -d, -f1-2 !{csv} | tail -n+2) - \
    > fitseq_out_!{rep}.csv
echo "rep",$(head -n1 fitseq_meanfit.csv) > fitseq_meanfit_!{rep}.csv 
tail -n+2 fitseq_meanfit.csv | awk '{print "!{rep},"$0 }' \
    >> fitseq_meanfit_!{rep}.csv 
'''
}
    //#cp variant_ids.csv variant_ids_!{parms_string}.csv



process assemble_sqlites {
    publishDir 'output', mode: 'rellink'
//    time '1h'
    label 'bioinfmunger'
    label 'one_core'
//    cpus '1'
//    label 'all_mem'
    time '8h'
    memory '4G'
        // experiment, rep, fitseq_outcsv, fitseqmeanfit, [unfilt tabulations], 
        //     [per-rep counts], [barcode_maps],  additional_files
    input: tuple val(experiment), val(reps), 
        path(fitseq_out_csv), path(fitseq_mean_csv),
        path(unfilt_tabulations), path(per_rep_counts),
        path(barcode_maps), path(additional_files)
    output: tuple val(experiment), val(reps), 
        path('d359_analysis.sqlite'), path(fitseq_mean_csv), 
        path(barcode_maps), path(additional_files)
    shell: '''
#
    echo "sticking unfiltered counts in database..."
    is_first=1
    for unfilt_file in !{unfilt_tabulations}; do
        cat ${unfilt_file} \
            | awk -F, 'BEGIN {firstline=1} \
                    { \
                    if (firstline==1) { \
                        split($0,columns,","); \
                        if ('${is_first}') {
                            print columns[1]",sample,counts"; \
                        }
                        firstline=0; \
                    } else { \
                        split($0,these_values,","); \
                        for (i in columns) { \
                            if (i==1) continue; \
                            print these_values[1]","columns[i]","these_values[i]; \
                        } \
                    } \
                    }' \
            | sqlite3 -csv d359_analysis.sqlite ".import '|cat -' unfiltered_melty"
        is_first=0
    done
    #sqlite3 d359_analysis.sqlite "create table unfiltered_sumd as select * from (select g,sample,sum(counts) as counts from unfiltered_melty where counts > 0 and g != 'g' group by g,sample) where counts > 0;"
    sqlite3 d359_analysis.sqlite "create table unfiltered_sumd as select g,sample,sum(counts) as counts from unfiltered_melty where g != 'g' group by g,sample;"
#
    echo "sticking per-rep counts in database..."
    is_first=1
    for counts_file in !{per_rep_counts}; do
        cat ${counts_file} \
            | awk -F, 'BEGIN {firstline=1} \
                    { \
                    if (firstline==1) { \
                        split($0,columns,","); \
                        if ('${is_first}') { \
                            print columns[1]","columns[2]",sample,counts"; \
                        }
                        firstline=0; \
                    } else { \
                        split($0,these_values,","); \
                        for (i in columns) { \
                            if (i==1 || i==2) continue; \
                            print these_values[1]","these_values[2]","columns[i]","these_values[i]; \
                        } \
                    } \
                    }' \
            | sqlite3 -csv d359_analysis.sqlite ".import '|cat -' rep_counts"
        is_first=0
    done
#
    echo "sticking fitseq modeled in database..."
    is_first=1
    for fitseq_out in !{fitseq_out_csv}; do
        cat ${fitseq_out} \
            | awk -F, 'BEGIN \
                    {firstline=1} \
                    { \
                    split($0,these_values,","); \
                    if (firstline==1 && '${is_first}') { \
                        printf "rep,g,fit,fit_se,ll"; \
                        for (i in these_values) { \
                            if (i<=4) continue; \
                            printf ",Modeled_Timepoint_"(i-5); \
                        } \
                        print ""; \
                        firstline=0; \
                    } \
                    else{ print $0; } \
                    }' \
            | sqlite3 -csv d359_analysis.sqlite ".import '|cat -' fitseq_out"
        is_first=1
    done
#
    echo "sticking forward barcode map in database..."
    cat !{barcode_maps[0]} \
        | awk -F, 'BEGIN {firstline=1} \
                { if (firstline==1) { print "which_code,"$0; firstline=0; } \
                else {print "forward,"$0;} }' \
        | sqlite3 -csv d359_analysis.sqlite ".import '|cat -' barcode_map"
#
    echo "sticking reverse barcode map in database..."
    cat !{barcode_maps[1]} \
        | awk -F, 'BEGIN {firstline=1} \
                { if (firstline==1) { print "which_code,"$0; firstline=0; } \
                else {print "reverse,"$0;} }' \
        | sqlite3 -csv d359_analysis.sqlite ".import '|cat -' barcode_map"
#
'''
}

process build_scratch_sqlite {
    publishDir 'output', mode: 'rellink'
    label 'r'
    label 'one_core'
    time '4h'
    memory '48G'
        // experiment, rep, fitseq_outcsv, fitseqmeanfit, [unfilt tabulations], 
        //     [per-rep counts], [barcode_maps], additional_files, script
    input: tuple val(experiment), val(reps), 
        path(sqlite), path(fitseq_mean_csv),
        path(barcode_maps), path(additional_files), path(script), 
        path(helperscript)
    output: tuple val(experiment), val(reps), 
        path(sqlite), path(fitseq_mean_csv), 
        path('scratch.sqlite')
    shell: '''
rm -rf $(dirname $(readlink !{script}))/$(basename $(readlink !{script}) .Rmd)_cache
Rscript -e "rmarkdown::render('!{script}',output_dir=getwd(),knit_root_dir=getwd())" \
    !{reps.sort{ a,b -> a.toString() <=> b.toString() }.join(',')} \
    !{sqlite} \
    !{fitseq_mean_csv.sort{ a,b -> a.toString() <=> b.toString() }.join(',')} \
    !{barcode_maps.sort{ a,b -> a.toString() <=> b.toString() }.join(' ')} \
    !{additional_files.join(' ')}
'''
}

process qc_filter_sqlite {
    publishDir 'output', mode: 'rellink'
//    time '2h'
    label 'r'
    label 'one_core'
//    cpus '1'
//    label 'all_mem'
    time '2h'
    memory '48G'
    input: tuple val(experiment), val(reps), 
        path(analysis_sqlite), path(fitseq_mean_csv), 
        path(scratch_sqlite), path(script),
        path(helperscript)
    output: tuple val(experiment), val(reps), 
        path(analysis_sqlite), path(fitseq_mean_csv), 
        path(scratch_sqlite), path("filtered.sqlite")
    shell: '''
rm -rf $(dirname $(readlink !{script}))/$(basename $(readlink !{script}) .Rmd)_cache
Rscript -e "rmarkdown::render('!{script}',output_dir=getwd(),knit_root_dir=getwd())" \
    !{fitseq_mean_csv.sort{ a,b -> a.toString() <=> b.toString() }.join(',')} \
    !{analysis_sqlite} \
    !{scratch_sqlite} 
'''
}

process call_d359 {
    publishDir 'output', mode: 'rellink'
    label 'r'
    label 'one_core'
//    cpus '1'
    time '4h'
    memory '48G'
    input: tuple val(experiment), val(reps), 
        path(analysis_sqlite), path(fitseq_mean_csv), 
        path(scratch_sqlite), path(filtered_sqlite),
        path(barcode_maps), path(script), path(helper_script)
    output: tuple val(experiment), path("d359_ppis_called*.csv"), 
        path(barcode_maps), path("*html"), path("*jpeg") //, path("*pdf")
    shell: '''
rm -rf $(dirname $(readlink !{script}))/$(basename $(readlink !{script}) .Rmd)_cache
Rscript -e "rmarkdown::render('!{script}',output_dir=getwd(),knit_root_dir=getwd())" \
    !{analysis_sqlite} \
    !{scratch_sqlite} \
    !{filtered_sqlite} \
    !{fitseq_mean_csv.sort{ a,b -> a.toString() <=> b.toString() }.join(',')} \
    !{barcode_maps.sort{ a,b -> a.toString() <=> b.toString() }.join(' ')} 
'''
}

process build_horf2go_csv {
    publishDir 'output', mode: 'rellink'
    label 'r'
    label 'one_core'
    label 'all_mem'
    input: tuple path(human_orfs_csv), path(script)
    output: path("horf2go.csv")
    shell: '''
rm -rf $(dirname $(readlink !{script}))/$(basename $(readlink !{script}) .Rmd)_cache
Rscript -e "rmarkdown::render('!{script}',output_dir=getwd(),knit_root_dir=getwd())" 
'''
}

process analyze_d360 {
    publishDir 'output' //, mode: 'copy'
    time '1h'
    label 'r'
    label 'one_core'
    memory '48G'
    input: tuple val(experiment), val(lane), path(tsv_gz), //path(umi_tsv_gz),
        path(script), path(additional_files)
    output: tuple val(experiment), path("*")
    shell: '''
rm -rf $(dirname $(readlink !{script}))/$(basename $(readlink !{script}) .Rmd)_cache
Rscript -e "rmarkdown::render('!{script}',output_dir=getwd(),knit_root_dir=getwd())" !{tsv_gz} !{additional_files.join(' ')}
'''
}



